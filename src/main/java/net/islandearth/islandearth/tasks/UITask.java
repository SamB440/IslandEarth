package net.islandearth.islandearth.tasks;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import lombok.AllArgsConstructor;
import net.islandearth.islandearth.IslandEarth;
import net.islandearth.islandearth.profile.Profile;
import net.islandearth.islandearth.ui.CharacterUI;

@AllArgsConstructor
public class UITask implements Runnable {
	
	private IslandEarth plugin;

	@Override
	public void run() {
		for(Player player : Bukkit.getOnlinePlayers())
		{
			Profile profile = plugin.getCache().getProfile(player.getUniqueId());
			if(profile != null)
			{
				if(profile.getCurrentCharacter() == null)
				{
					if(player.getOpenInventory().getType() == InventoryType.CRAFTING) new CharacterUI(plugin, profile).openInventory(plugin, player);
				}
			}
		}
	}
}
