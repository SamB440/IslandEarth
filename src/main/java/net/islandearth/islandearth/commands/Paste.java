package net.islandearth.islandearth.commands;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import net.islandearth.islandearth.IslandEarth;
import net.islandearth.islandearth.utils.Schematic;

public class Paste extends BukkitCommand {
	
	private IslandEarth plugin;
	
	public Paste(IslandEarth plugin)
	{
		super("Paste");
		this.description = "Paste Schematic Test";
		this.usageMessage = "/Paste";
		this.plugin = plugin;
	}

	@Override
	public boolean execute(CommandSender sender, String argd, String[] args) {
		if(sender instanceof Player)
		{
			if(sender.isOp())
			{
				if(args.length == 1)
				{
					Player player = (Player) sender;
					File target = new File(Bukkit.getPluginManager().getPlugin("WorldEdit").getDataFolder() + "/schematics/" + args[0] + ".schem");
					if(!target.exists())
					{
						System.out.println("Target file was:" + target.getPath());
						player.sendMessage(ChatColor.RED + "Invalid schematic supplied: /paste <schematic>");
						return true;
					}
					
					Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
						Schematic schematic = new Schematic(plugin, target);
						plugin.getCache().addBuilding(player, schematic);
					});
				} else if(args.length == 2) {
					if(args[1].equalsIgnoreCase("true") || args[1].equalsIgnoreCase("false"))
					{
						Player player = (Player) sender;
						File target = new File(Bukkit.getPluginManager().getPlugin("WorldEdit").getDataFolder() + "/schematics/" + args[0] + ".schem");
						if(!target.exists())
						{
							System.out.println("Target file was:" + target.getPath());
							player.sendMessage(ChatColor.RED + "Invalid schematic supplied: /paste <schematic>");
							return true;
						}
						
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							Schematic schematic = new Schematic(plugin, target);
							schematic.pasteSchematic(player.getTargetBlock(null, 7).getLocation().add(0, 1, 0), player, Boolean.valueOf(args[1]), 10);
						});
					} else sender.sendMessage(ChatColor.RED + "Invalid schematic supplied: /paste <schematic>");
				} else sender.sendMessage(ChatColor.RED + "Invalid schematic supplied: /paste <schematic>");
			} else sender.sendMessage(ChatColor.RED + "Error: You do not have permission to do this.");
		} return true;
	}
}
