package net.islandearth.islandearth.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;

public class Help extends BukkitCommand {

	public Help() 
	{
		super("Help");
		this.description = "Help";
		this.usageMessage = "/Help";
	}
	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {
		switch(args.length)
		{
			default:
				sender.sendMessage(ChatColor.BLUE + "      IslandEarth Help" + ChatColor.YELLOW + " (1/1)" + "      ");
				sender.sendMessage(ChatColor.GREEN + "Website " + ChatColor.YELLOW + "www.islandearth.net");
				sender.sendMessage(ChatColor.GREEN + "Store " + ChatColor.YELLOW + "www.islandearth.net/store.html");
				sender.sendMessage(ChatColor.GREEN + "Discord " + ChatColor.YELLOW + "www.discord.gg/fh62mxU");
				break;
		} return true;
	}
}
