package net.islandearth.islandearth.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;

public class IslandEarth_Command extends BukkitCommand {

	public IslandEarth_Command() 
	{
		super("IslandEarth");
		this.description = "IslandEarth";
		this.usageMessage = "/IslandEarth";
	}

	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {
		sender.sendMessage(ChatColor.GREEN + "© 2018 IslandEarth. All rights reserved.");
		return true;
	}
}
