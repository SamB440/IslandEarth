package net.islandearth.islandearth.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;

public class Discord extends BukkitCommand {

	public Discord() 
	{
		super("Discord");
		this.description = "Discord URL";
		this.usageMessage = "/Discord";
	}
	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {
		sender.sendMessage(ChatColor.GREEN + "www.discord.gg/fh62mxU");
		return true;
	}
}
