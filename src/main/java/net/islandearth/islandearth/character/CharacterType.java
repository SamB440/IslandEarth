package net.islandearth.islandearth.character;

import org.bukkit.Material;

import lombok.Getter;

public enum CharacterType {
	MAGE(Material.STICK);
	
	@Getter private Material material;
	
	CharacterType(Material material)
	{
		this.material = material;
	}
}
