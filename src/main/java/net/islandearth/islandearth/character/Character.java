package net.islandearth.islandearth.character;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Character {

	@Getter @Setter private int xp;
	@Getter @Setter private int level;
	@Getter @Setter private String name;
	@Getter @Setter private List<ItemStack> items;
	@Getter @Setter private CharacterType type;
	@Getter @Setter private Location location;
}
