package net.islandearth.islandearth;

import java.io.File;
import java.lang.reflect.Field;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandMap;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import lombok.Getter;
import net.islandearth.islandearth.commands.Discord;
import net.islandearth.islandearth.commands.Help;
import net.islandearth.islandearth.commands.IslandEarth_Command;
import net.islandearth.islandearth.commands.Paste;
import net.islandearth.islandearth.listeners.PlayerListener;
import net.islandearth.islandearth.profile.Profile;
import net.islandearth.islandearth.sql.ConnectionManager;
import net.islandearth.islandearth.tasks.BuildTask;
import net.islandearth.islandearth.tasks.UITask;
import net.islandearth.islandearth.utils.Cache;
import net.islandearth.queste.Queste;
import net.milkbowl.vault.economy.Economy;

public class IslandEarth extends JavaPlugin {
	
	@Getter private Queste queste;
	@Getter private Cache cache;
	@Getter private Economy economy;
	@Getter private ConnectionManager connectionManager;
	@Getter private BuildTask buildTask;
	@Getter private Scoreboard scoreboard;
	@Getter private String[] ranks;
	
	@Override
	public void onEnable()
	{
		createConfig();
		setupEconomy();
		this.queste = (Queste) Bukkit.getPluginManager().getPlugin("queste");
		this.cache = new Cache();
		this.ranks = new String[] {ChatColor.RED + "Emperor", ChatColor.GREEN + "Explorer", ChatColor.GREEN + "Traveller"};
		this.scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
		for(String rank : ranks)
		{
			Team team = scoreboard.registerNewTeam(ChatColor.stripColor(rank));
			team.setPrefix(rank + " ");
		}
		this.connectionManager = new ConnectionManager(null, 
				getConfig().getString("host"), 
				getConfig().getString("database"), 
				getConfig().getString("username"), 
				getConfig().getString("password"), 
				getConfig().getInt("port"));
		try {
			connectionManager.openConnection();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			getLogger().severe("[IslandEarth] Could not connect to database.");
		}
		
		registerListeners();
		registerCommands();
		startTasks();
	}
	
	@Override
	public void onDisable()
	{
		for(Player player : Bukkit.getOnlinePlayers())
		{
			
			Profile profile = getCache().getProfile(player.getUniqueId());
			profile.getCurrentCharacter().getItems().clear();
			
			for(ItemStack item : player.getInventory().getContents())
			{
				profile.getCurrentCharacter().getItems().add(item);
			}
			
			profile.saveData();
			getCache().removeProfile(profile);
			getLogger().info("[IslandEarth] Saved profile data for " + player.getName() + ".");
		}
	}
	
	private void createConfig()
	{
		if(!new File(getDataFolder() + "/getConfig().yml").exists()) saveDefaultConfig();
	}
	
	private void registerListeners()
	{
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new PlayerListener(this), this);
	}
	
	private void registerCommands()
	{
		try {
			Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
			bukkitCommandMap.setAccessible(true);
			CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());

			commandMap.register("Paste", new Paste(this));
			commandMap.register("IslandEarth", new IslandEarth_Command());
			commandMap.register("Help", new Help());
			commandMap.register("Discord", new Discord());
		} catch (NoSuchFieldException | 
				SecurityException | 
				IllegalArgumentException | 
				IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
    private boolean setupEconomy() 
    {
        if(getServer().getPluginManager().getPlugin("Vault") == null) return false;
        
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if(rsp == null) return false;
        economy = rsp.getProvider();
        return economy != null;
    }
    
	@SuppressWarnings("deprecation")
	private void startTasks()
	{
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new UITask(this), 20L, 40L);
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(this, buildTask = new BuildTask(this), 20L, 1L);
	}
}
