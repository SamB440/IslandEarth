package net.islandearth.islandearth.ui;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.islandearth.islandearth.IslandEarth;
import net.islandearth.islandearth.character.Character;
import net.islandearth.islandearth.profile.Profile;

public class CharacterUI extends UI {

	public CharacterUI(IslandEarth plugin, Profile profile) 
	{
		super(18, "Select a Character");
		
		ItemStack blank = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
		ItemMeta bm = blank.getItemMeta();
		bm.setDisplayName(ChatColor.GREEN + "[+] Create a new character");
		blank.setItemMeta(bm);
		
		int current = 2;
		
		for(Character character : profile.getCharacters())
		{
			ItemStack item = new ItemStack(character.getType().getMaterial());
			ItemMeta im = item.getItemMeta();
			im.setDisplayName(ChatColor.GOLD + "[>] Select This Character");
			im.setLore(Arrays.asList(ChatColor.YELLOW + "Character Info:",
					ChatColor.YELLOW + "- " + ChatColor.GRAY + "Class: " + ChatColor.WHITE + StringUtils.capitalize(character.getType().toString().toLowerCase().replace("_", " ")),
					ChatColor.YELLOW + "- " + ChatColor.GRAY + "Level: " + ChatColor.WHITE + character.getLevel(),
					ChatColor.YELLOW + "- " + ChatColor.GRAY + "XP: " + ChatColor.WHITE + character.getXp(),
					ChatColor.YELLOW + "- " + ChatColor.GRAY + "Started / Finished Quests: " + ChatColor.WHITE + plugin.getQueste().getCache().getProfile(profile.getPlayer().getUniqueId()).getCompletedQuests().size() + "/" + plugin.getQueste().getCache().getQuests().size()));
			item.setItemMeta(im);
			
			setItem(current, item, player -> {
				player.getInventory().clear();
				player.setLevel(0);
				player.setExp(0);
				player.setLevel(character.getLevel());
				player.giveExp(character.getXp());
				player.getInventory().setContents(character.getItems().toArray(new ItemStack[character.getItems().size()]));
				player.teleport(character.getLocation());
				player.closeInventory();
				
				profile.setCurrentCharacter(character);
			});
			current++;
		}
		
		for(int i = current; i <= 6; i++)
		{
			setItem(i, blank, player -> {
				new CreateUI(profile).openInventory(plugin, player);
			});
		}
	}
}
