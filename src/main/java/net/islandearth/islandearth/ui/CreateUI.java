package net.islandearth.islandearth.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.islandearth.islandearth.character.Character;
import net.islandearth.islandearth.character.CharacterType;
import net.islandearth.islandearth.profile.Profile;

public class CreateUI extends UI {
	
	public CreateUI(Profile profile)
	{
		super(18, "Create Your Character");
		
		ItemStack mage = new ItemStack(Material.STICK);
		ItemMeta mm = mage.getItemMeta();
		mm.setDisplayName(ChatColor.WHITE + "Select " + ChatColor.GOLD + ChatColor.BOLD + "Mage");
		mage.setItemMeta(mm);
		
		setItem(1, mage, player -> {
			List<CharacterType> types = new ArrayList<>();
			for(Character character : profile.getCharacters())
			{
				types.add(character.getType());
			}
			
			if(types.contains(CharacterType.MAGE))
			{
				player.sendMessage(ChatColor.RED + "You already own a class of that type!");
				return;
			} else {
				player.teleport(new Location(player.getWorld(), 0, 100, 0));
				player.closeInventory();
				
				ItemStack stick = new ItemStack(Material.STICK);
				ItemMeta sm = stick.getItemMeta();
				sm.setDisplayName(ChatColor.WHITE + "Wand");
				stick.setItemMeta(sm);
				
				player.getInventory().addItem(stick);
				
				Character character = new Character(0,
						0,
						"Mage",
						Arrays.asList(player.getInventory().getContents()),
						CharacterType.MAGE,
						new Location(player.getWorld(), 0, 100, 0));
				
				profile.setCurrentCharacter(character);
				profile.getCharacters().add(character);
			}
		});
	}
}
