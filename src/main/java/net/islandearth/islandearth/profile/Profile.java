package net.islandearth.islandearth.profile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import lombok.Setter;
import net.islandearth.islandearth.IslandEarth;
import net.islandearth.islandearth.character.Character;
import net.islandearth.islandearth.character.CharacterType;;

public class Profile {
	
	@Getter private Player player;
	@Getter private File file;
	@Getter private FileConfiguration config;
	@Getter private List<Character> characters;
	@Getter @Setter private Character currentCharacter;
	
	@SuppressWarnings("unchecked")
	public Profile(IslandEarth plugin, Player player)
	{
		this.player = player;
		this.file = new File(plugin.getDataFolder() + "/profiles/" + player.getUniqueId().toString() + ".profile");
		this.config = YamlConfiguration.loadConfiguration(file);
		
		List<Character> current = new ArrayList<>();
		
		for(String string : config.getStringList("Characters"))
		{
			current.add(new Character(config.getInt(string + ".xp"),
					config.getInt(string + ".level"),
					config.getString(string + ".name"),
					(List<ItemStack>) config.getList(string + ".items"),
					CharacterType.valueOf(config.getString(string + ".type")),
					new Location(Bukkit.getWorld(config.getString(string + ".location.world")), config.getInt(string + ".location.x"), config.getInt(string + ".location.y"), config.getInt(string + ".location.z"))));
		} this.characters = current;
	}
	
	public void saveData()
	{
		for(Character character : characters) 
		{
			List<String> current = config.getStringList("Characters");
			current.add(character.getName());
		    config.set("Characters", current);
		    config.set(character.getName() + ".xp", character.getXp());
		    config.set(character.getName() + ".level", character.getLevel());
		    config.set(character.getName() + ".type", character.getType().toString());
		    config.set(character.getName() + ".items", character.getItems());
		    config.set(character.getName() + ".location.world", character.getLocation().getWorld().getName());
		    config.set(character.getName() + ".location.x", character.getLocation().getBlockX());
		    config.set(character.getName() + ".location.y", character.getLocation().getBlockY());
		    config.set(character.getName() + ".location.z", character.getLocation().getBlockZ());
		} reloadConfiguration();
	}
	
	public void reloadConfiguration()
	{
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
