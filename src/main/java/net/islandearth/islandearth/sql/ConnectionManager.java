package net.islandearth.islandearth.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class ConnectionManager {
	
	@Getter private Connection connection;
	protected String host, database, username, password;
	protected int port;
	
	public void openConnection() throws SQLException, ClassNotFoundException
	{
		if(connection != null && !connection.isClosed()) return;
		
		synchronized(this)
		{
			if(connection != null && !connection.isClosed()) return;
			Class.forName("com.mysql.jdbc.Driver");
	        connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database, this.username, this.password);
		}
	}
	
	public void closeConnection() throws SQLException
	{
		connection.close();
	}
}
