package net.islandearth.islandearth.listeners;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import lombok.AllArgsConstructor;
import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.User;
import net.islandearth.islandearth.IslandEarth;
import net.islandearth.islandearth.utils.Scheduler;
import net.islandearth.islandearth.utils.Schematic;

@AllArgsConstructor
public class PlayerListener implements Listener {
	
	private IslandEarth plugin;
	
	/*@EventHandler
	public void onStart(QuestStartEvent qse)
	{
		setQuestBook(qse.getPlayer());
	}
	
	@EventHandler
	public void onComplete(QuestCompleteEvent qce)
	{
		setQuestBook(qce.getPlayer());
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent pje)
	{
		if(plugin.getCache().getProfile(pje.getPlayer().getUniqueId()) == null) plugin.getCache().addProfile(new Profile(plugin, pje.getPlayer()));
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
			setQuestBook(pje.getPlayer());
			new CharacterUI(plugin, plugin.getCache().getProfile(pje.getPlayer().getUniqueId())).openInventory(plugin, pje.getPlayer());
		}, 20);
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent pqe)
	{
		items.remove(pqe.getPlayer().getUniqueId());
		
		Profile profile = plugin.getCache().getProfile(pqe.getPlayer().getUniqueId());
		if(profile.getCurrentCharacter() != null)
		{
			profile.getCurrentCharacter().getItems().clear();
			
			for(ItemStack item : pqe.getPlayer().getInventory().getContents())
			{
				profile.getCurrentCharacter().getItems().add(item);
			}
		}
		
		Bukkit.getScheduler().runTaskLater(plugin, () -> {
			profile.saveData();
			plugin.getCache().removeProfile(profile);
			plugin.getLogger().info("[IslandEarth] Saved profile data for " + pqe.getPlayer().getName() + ".");
		}, 20);
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent pde)
	{
		pde.setKeepInventory(true);
		pde.setKeepLevel(true);
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent pdie)
	{
		if(pdie.getPlayer().getInventory().getHeldItemSlot() == 8) pdie.setCancelled(true);
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent ice)
	{
		if(ice.getCursor() != null)
		{
			if(ice.getCursor().equals(items.get(ice.getWhoClicked().getUniqueId()))) ice.setCancelled(true);
		}
		
		if(ice.getWhoClicked() instanceof Player)
		{
			Player player = (Player) ice.getWhoClicked();
			UUID uuid = UI.getOpen().get(player.getUniqueId());
			if(uuid != null)
			{
				ice.setCancelled(true);
				UI ui = UI.getInventories().get(uuid);
				ItemClick action = ui.getActions().get(ice.getSlot());
				
				if(action != null)
				{
					action.click(player);
				}
			}
		}
	}
	
	@EventHandler
	public void onMove(InventoryMoveItemEvent imie)
	{
		if(imie.getSource().getHolder() instanceof Player)
		{
			if(imie.getItem().equals(items.get(((Player) imie.getSource().getHolder()).getUniqueId()))) imie.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onClose(InventoryCloseEvent ice)
	{
		if(ice.getPlayer() instanceof Player)
		{
			Player player = (Player) ice.getPlayer();
			UI ui = UI.getInventories().get(UI.getOpen().get(player.getUniqueId()));
			if(ui != null) ui.delete();
			if(UI.getOpen().containsKey(player.getUniqueId())) UI.getOpen().remove(player.getUniqueId());
		}
	}*/
	
	@EventHandler
	public void onJoin(PlayerJoinEvent pje)
	{
		Player player = pje.getPlayer();
		User user = LuckPerms.getApi().getUser(player.getUniqueId());
		String prefix = user.getPrimaryGroup();
		List<String> ranks = new ArrayList<>();
		Arrays.asList(plugin.getRanks()).forEach(rank -> ranks.add(ChatColor.stripColor(rank.toLowerCase())));
		if(ranks.contains(ChatColor.stripColor(prefix)))
		{
			plugin.getScoreboard().getTeam(StringUtils.capitalize(ChatColor.stripColor(prefix))).addEntry(player.getName());
			for(Player players : Bukkit.getOnlinePlayers())
			{
				players.setScoreboard(plugin.getScoreboard());
			}
		} else {
			plugin.getScoreboard().getTeam("Traveller").addEntry(player.getName());
			for(Player players : Bukkit.getOnlinePlayers())
			{
				players.setScoreboard(plugin.getScoreboard());
			}
		}
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent pqe)
	{
		Player player = pqe.getPlayer();
		User user = LuckPerms.getApi().getUser(player.getUniqueId());
		String prefix = user.getPrimaryGroup();
		List<String> ranks = new ArrayList<>();
		Arrays.asList(plugin.getRanks()).forEach(rank -> ranks.add(ChatColor.stripColor(rank.toLowerCase())));
		plugin.getScoreboard().getTeam(StringUtils.capitalize(ChatColor.stripColor(prefix))).removeEntry(player.getName());
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent pie)
	{
		Player player = pie.getPlayer();
		if(pie.getAction() == Action.RIGHT_CLICK_BLOCK || pie.getAction() == Action.RIGHT_CLICK_AIR)
		{
			if(plugin.getCache().isBuilding(player))
			{
				Schematic schematic = plugin.getCache().getBuilding(player);
				List<Location> locations = schematic.pasteSchematic(player.getTargetBlock(null, 7).getLocation().add(0, 1, 0), player, false, 10);
				if(locations != null)
				{
					
					if(plugin.getBuildTask().getCache().containsKey(player))
					{
						for(Location clear : plugin.getBuildTask().getCache().get(player))
						{
							if(clear.getBlock().getType() == Material.AIR) player.sendBlockChange(clear, Material.AIR.createBlockData());
						}
					}
				
					Scheduler scheduler = new Scheduler();
					scheduler.setTask(Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
						for(Location loc : locations)
						{
							if(loc.getBlock().getType() == Material.AIR) loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc.getX() + 0.5, loc.getY(), loc.getZ() + 0.5, 2);
						}
						
						if(locations.get(locations.size() - 1).getBlock().getType() != Material.AIR || schematic.isPasted())
						{
							scheduler.cancel();
							schematic.setPasted(true);
						}
					}, 0, 40));
					
					player.sendMessage(ChatColor.GREEN + "Schematic is now being built!");
					plugin.getCache().removeBuilding(player);
					
				} else {
					player.sendMessage(ChatColor.RED + "You can't build that here!");
				}
			}
		} else if(pie.getAction() == Action.LEFT_CLICK_BLOCK || pie.getAction() == Action.LEFT_CLICK_AIR) {
			
			if(plugin.getCache().isBuilding(player))
			{
				for(Location clear : plugin.getBuildTask().getCache().get(player))
				{
					if(clear.getBlock().getType() == Material.AIR) player.sendBlockChange(clear, Material.AIR.createBlockData());
				}
				
				player.sendMessage(ChatColor.RED + "Cancelled building placement.");
				plugin.getCache().removeBuilding(player);
			}
		}
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent ace)
	{
		ace.setCancelled(true);
		Player player = ace.getPlayer();
		String format = null;
		User user = LuckPerms.getApi().getUser(player.getUniqueId());
		if(player.isOp() || user.getPrimaryGroup().equalsIgnoreCase("Emperor")) format = ChatColor.RED + "Emperor " + ChatColor.WHITE + player.getName() + ChatColor.GRAY + ": " + ChatColor.WHITE + ace.getMessage();
		else if(user.getPrimaryGroup().equalsIgnoreCase("Explorer")) format = ChatColor.GREEN + "Explorer " + ChatColor.WHITE + player.getName() + ChatColor.GRAY + ": " + ace.getMessage();
		else format = ChatColor.GREEN + "Traveller " + ChatColor.WHITE + player.getName() + ChatColor.GRAY + ": " + ace.getMessage();
		for(Player players : Bukkit.getOnlinePlayers())
		{
			players.sendMessage(format);
		}
	}
	
	/*private void setQuestBook(Player player)
	{
		if(plugin.getQueste().getCache().getProfile(player.getUniqueId()).getCurrentQuests().keySet().isEmpty())
		{
			ItemStack qb = new ItemStack(Material.WRITTEN_BOOK);
			BookMeta qbm = (BookMeta) qb.getItemMeta();
			
			BaseComponent[] page = new ComponentBuilder("\n\n\n\n\nYou are not doing any\n  quests at this time.")
			        .create();
			
			qbm.spigot().addPage(page);
			qbm.setTitle(ChatColor.GOLD + "Quests");
			qbm.setAuthor("SamB440");
			
			qb.setItemMeta(qbm);
			
			items.put(player.getUniqueId(), qb);
			
			player.getInventory().setItem(8, qb);
		} else {
			ItemStack qb = new ItemStack(Material.WRITTEN_BOOK);
			BookMeta qbm = (BookMeta) qb.getItemMeta();
			for(String string : plugin.getQueste().getCache().getProfile(player.getUniqueId()).getCurrentQuests().keySet())
			{
				Quest quest = plugin.getQueste().getCache().getQuest(string);
				if(quest != null)
				{
					
					BaseComponent[] page = new ComponentBuilder(ChatColor.GOLD + "        " + ChatColor.UNDERLINE + quest.getName())
					        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.GREEN + "£" + quest.getMoney() + "\n" + ChatColor.AQUA + quest.getXp() + "XP" + "\n" + quest.getDescription().toString()).create()))
					        .create();
					
					qbm.spigot().addPage(page);
				}
			}
			
			qbm.setTitle(ChatColor.GOLD + "Quests");
			qbm.setAuthor("SamB440");
			
			qb.setItemMeta(qbm);
			
			items.put(player.getUniqueId(), qb);
			
			player.getInventory().setItem(8, qb);
		}
	}*/
}
