package net.islandearth.islandearth.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;

import lombok.Getter;
import lombok.Setter;

public class Scheduler {
	
	@Getter @Setter private int task;
	@Getter private List<Object> values = new ArrayList<Object>();

	public void cancel()
	{
		Bukkit.getScheduler().cancelTask(task);
	}
	
	public void addValue(Object object)
	{
		values.add(object);
	}
	
	public void removeValue(Object object)
	{
		values.remove(object);
	}
	
	public void setValue(int index, Object object)
	{
		values.set(index, object);
	}
	
	public Object getValue(int index)
	{
		return values.get(index);
	}
}
