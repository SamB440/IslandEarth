package net.islandearth.islandearth.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;

import net.islandearth.islandearth.profile.Profile;

public class Cache {
	
	private Map<UUID, Profile> profiles = new HashMap<>();
	private Map<UUID, Schematic> building = new HashMap<>();
	
	public Profile getProfile(UUID uuid)
	{
		if(profiles.containsKey(uuid)) return profiles.get(uuid);
		else return null;
	}
	
	public void addProfile(Profile profile)
	{
		profiles.put(profile.getPlayer().getUniqueId(), profile);
	}
	
	public void removeProfile(Profile profile)
	{
		profiles.remove(profile.getPlayer().getUniqueId());
	}
	
	public boolean isBuilding(Player player)
	{
		return building.containsKey(player.getUniqueId());
	}
	
	public void addBuilding(Player player, Schematic schematic)
	{
		building.put(player.getUniqueId(), schematic);
	}
	
	public void removeBuilding(Player player)
	{
		building.remove(player.getUniqueId());
	}
	
	public Schematic getBuilding(Player player)
	{
		return building.get(player.getUniqueId());
	}
}
